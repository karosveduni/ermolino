﻿using Ermolino.Model;
using Ermolino.Model.ClassesForCommunicationWithOtherClasses;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace Ermolino.ViewModel
{
    class HomeViewModel : BaseViewModel
    {
        public TappedEventHandler FrameTapped => (sender, e) => 
        {
            if(sender is Frame frame && frame.Content is StackPanel stack && stack.Children[1] is TextBlock text)
            {
                HomeAndProduct.LoadProduct(text.Text);
            }
        };
    }
}
