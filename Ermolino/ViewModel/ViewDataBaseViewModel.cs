﻿using Ermolino.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Ermolino.ViewModel
{
    class ViewDataBaseViewModel : BaseViewModel
    {
        public ViewDataBaseViewModel()
        {
            ButtonAllProduct.Execute(null);
        }
        public bool IsActive { get; set; } = false;
        public Visibility VisibilityFrame { get; set; } = Visibility.Collapsed;
        public Visibility VisibilityGrid { get; set; } = Visibility.Visible;
        public Visibility VisibilityGridUser { get; set; } = Visibility.Collapsed;
        public Visibility VisibilityButtonAddAndCancelAdd { get; set; } = Visibility.Collapsed;
        public Visibility VisibilitySplitButton { get; set; } = Visibility.Visible;
        public Visibility VisibilityTypeProduct { get; set; } = Visibility.Collapsed;
        private List<Ermolino64DLL.Model.Product> Products { get; set; }
        private Ermolino64DLL.Model.User[] Users { get; set; }
        public ObservableCollection<int> Index { get; set; } = new ObservableCollection<int>();
        public Ermolino64DLL.Model.Product SelectProduct { get; set; }
        public Ermolino64DLL.Model.User SelectUser { get; set; }
        public bool IsReadOnly { get; set; } = true;
        public string ButtonEditContext { get; set; } = "Изменить запись";
        public Image ImageSource { get; set; } = new Image();
        public bool IsEnabledRemoveButton { get; set; } = true;
        public bool IsChecked180 { get; set; } = false;
        public bool IsChecked90 { get; set; } = false;
        public bool IsChecked30 { get; set; } = false;
        public TextBlock ComboBoxSelectValue { get; set; }

        public ICommand ButtonLoadProduct => new Command((o) => 
        {
            SetVisibility(true);
            IndexInitialization(Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Product.Where(p => p.TypeProduct == o as string).Select(p => p.Id));
        });

        public ICommand ButtonAllProduct => new Command(async () => 
        {
            SetVisibility(true);

            VisibilityGrid = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityGrid));

            VisibilityFrame = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityFrame));
            IsActive = true;
            OnPropertyChanged(nameof(IsActive));

            Products = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Product.ToListAsync();

            Index.Clear();
            foreach (var item in Products)
            {
                Index.Add(item.Id);
                item.ProductAndShelfLife = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().ProductAndShelfLife.Where(pasl => pasl.IdProduct == item.Id).ToListAsync();
                foreach (var item1 in item.ProductAndShelfLife)
                {
                    item1.IdShelfLifeNavigation = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().ShelfLife.Where(sl => sl.Id == item1.IdShelfLife).FirstOrDefaultAsync();
                }
            }
            SetIsCheched(Products[0]);

            SelectProduct = Products[0];
            OnPropertyChanged(nameof(SelectProduct));
            CheckImage(SelectProduct.Image);

            VisibilityFrame = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityFrame));
            IsActive = false;
            OnPropertyChanged(nameof(IsActive));

            VisibilityGrid = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityGrid));
        });

        public SelectionChangedEventHandler SelectItemListBox => (sender, e) => 
        {
            if(sender is ListBox listBox && listBox.SelectedItem is int index)
            {
                SelectProduct = Products[index - 1];
                SetIsCheched(SelectProduct);
                OnPropertyChanged(nameof(SelectProduct));
                CheckImage(SelectProduct.Image);
            }
        };

        public ICommand ButtonRemove => new Command(async () => 
        {
            MessageDialog message = new MessageDialog("Вы подтверждаете удаление обекта без восможнности его востановить ?", "Удаление");

            message.Commands.Add(new UICommand("Удалить объект"));
            message.Commands.Add(new UICommand("Отменить Удаление"));

            var result = await message.ShowAsync();
            if (result.Label == "Удалить объект")
            {
                if(IsChecked180)
                    Ermolino64DLL.Model.ProductAndShelfLife.DeleteFromDataBase(SelectProduct, 1);
                await Task.Delay(100);
                if (IsChecked90)
                    Ermolino64DLL.Model.ProductAndShelfLife.DeleteFromDataBase(SelectProduct, 2);
                await Task.Delay(100);
                if (IsChecked30)
                    Ermolino64DLL.Model.ProductAndShelfLife.DeleteFromDataBase(SelectProduct, 3);
                await Task.Delay(100);

                Ermolino64DLL.Model.Product.RemoveFromDataBase(SelectProduct);

                Index.Remove(SelectProduct.Id);
                Products.Remove(SelectProduct);

                SelectProduct = Products[Index[Index.Count - 1] - 1];
                OnPropertyChanged(nameof(SelectProduct));

                CheckImage(SelectProduct.Image);
            }            
        });

        public ICommand ButtonEdit => new Command(() => 
        {
            if(ButtonEditContext == "Изменить запись")
            {
                IsReadOnly = false;
                OnPropertyChanged(nameof(IsReadOnly));
                ButtonEditContext = "Сохранить запись";
                OnPropertyChanged(nameof(ButtonEditContext));
                IsEnabledRemoveButton = false;
                OnPropertyChanged(nameof(IsEnabledRemoveButton));
            }
            else
            {
                IsReadOnly = true;
                OnPropertyChanged(nameof(IsReadOnly));
                ButtonEditContext = "Изменить запись";
                OnPropertyChanged(nameof(ButtonEditContext));

                if (IsChecked180)
                    Ermolino64DLL.Model.ProductAndShelfLife.AddInDataBase(SelectProduct, 1);
                else
                {
                    var value = SelectProduct.ProductAndShelfLife.FirstOrDefault(pasl => pasl.IdShelfLife == 1);
                    if(value != null)
                    SelectProduct.ProductAndShelfLife.Remove(value);
                    Ermolino64DLL.Model.ProductAndShelfLife.DeleteFromDataBase(SelectProduct, 1);   
                }

                if (IsChecked90)
                    Ermolino64DLL.Model.ProductAndShelfLife.AddInDataBase(SelectProduct, 2);
                else
                {
                    var value = SelectProduct.ProductAndShelfLife.FirstOrDefault(pasl => pasl.IdShelfLife == 2);
                    if (value != null)
                        SelectProduct.ProductAndShelfLife.Remove(value);
                    Ermolino64DLL.Model.ProductAndShelfLife.DeleteFromDataBase(SelectProduct, 2);
                }
                if (IsChecked30)
                    Ermolino64DLL.Model.ProductAndShelfLife.AddInDataBase(SelectProduct, 3);
                else
                {
                    var value = SelectProduct.ProductAndShelfLife.FirstOrDefault(pasl => pasl.IdShelfLife == 3);
                    if (value != null)
                        SelectProduct.ProductAndShelfLife.Remove(value);
                    Ermolino64DLL.Model.ProductAndShelfLife.DeleteFromDataBase(SelectProduct, 3);
                }

                

                Ermolino64DLL.Model.Product.UpdateDataBase(SelectProduct);

                IsEnabledRemoveButton = true;
                OnPropertyChanged(nameof(IsEnabledRemoveButton));
            }
        });

        public ICommand ButtonLoadImage => new Command(async () =>
        {
            FileOpenPicker fileOpenPicker = new FileOpenPicker()
            {
                ViewMode = PickerViewMode.List,
            };
            fileOpenPicker.FileTypeFilter.Add(".jpg");
            fileOpenPicker.FileTypeFilter.Add(".jpeg");
            fileOpenPicker.FileTypeFilter.Add(".png");

            var File = await fileOpenPicker.PickSingleFileAsync();

            if (File == null) return;
            using (var inputStream = await File.OpenSequentialReadAsync())
            {
                var readStream = inputStream.AsStreamForRead();
                var bytearray = new byte[readStream.Length];

                await readStream.ReadAsync(bytearray, 0, bytearray.Length);

                SelectProduct.Image = bytearray;
            }

            using (var filestream = await File.OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(filestream);
                ImageSource.Source = bitmapImage;
            }
            OnPropertyChanged(nameof(ImageSource));
        });

        public static async Task<BitmapImage> ByteArrayConvertToImageSource(byte[] bytes)
        {
            BitmapImage bitmapImage = new BitmapImage();
            using (InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream())
            {
                using (DataWriter writer = new DataWriter(stream.GetOutputStreamAt(0)))
                {
                    writer.WriteBytes(bytes);
                    await writer.StoreAsync();
                }
                await bitmapImage.SetSourceAsync(stream);
            }

            return bitmapImage;

        }

        private async void CheckImage(byte[] Image)
        {
            if (Image != null)
            {
                ImageSource.Source = await ByteArrayConvertToImageSource(Image);
                OnPropertyChanged(nameof(ImageSource));
            }
            else
            {
                ImageSource.Source = null;
                OnPropertyChanged(nameof(ImageSource));
            }
        }

        private void IndexInitialization(IEnumerable<int> index)
        {
            Index = new ObservableCollection<int>(index);
            OnPropertyChanged(nameof(Index));

            SelectProduct = Products[Index[0] - 1];
            SetIsCheched(SelectProduct);
            OnPropertyChanged(nameof(SelectProduct));

            CheckImage(SelectProduct.Image);
        }

        public ICommand ButtonAllUser => new Command(() => 
        {
            SetVisibility(false);

            VisibilityGridUser = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityGridUser));

            VisibilityFrame = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityFrame));
            IsActive = true;
            OnPropertyChanged(nameof(IsActive));

            Users = Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().User.ToArray();
            Index = new ObservableCollection<int>(Users.Select(u => u.Id));
            OnPropertyChanged(nameof(Index));

            SelectUser = Users[Index[0] - 1];
            OnPropertyChanged(nameof(SelectUser));

            CheckImage(SelectUser.Image);

            VisibilityFrame = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityFrame));
            IsActive = false;
            OnPropertyChanged(nameof(IsActive));

            VisibilityGridUser = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityGridUser));
        });

        public ICommand ButtonLoadUser => new Command((o) =>
        {

            if(Users == null)
            {
                ButtonAllUser.Execute(null);
            }
            
            Index = new ObservableCollection<int>(Users.Where(u => u.IsAdmin == bool.Parse(o as string)).Select(u => u.Id));
            OnPropertyChanged(nameof(Index));
            
           
            SelectUser = Users[Index[0] - 1];
            OnPropertyChanged(nameof(SelectUser));  

            CheckImage(SelectUser.Image);
        });

        public SelectionChangedEventHandler SelectItemListBoxUser => (sender, e) =>
        {
            if (sender is ListBox listBox && listBox.SelectedItem is int index)
            {
                SelectUser = Users[index - 1];
                OnPropertyChanged(nameof(SelectProduct));
                CheckImage(SelectUser.Image);
            }
        };

        private void SetVisibility(bool Product)
        {
            if(Product)
            {
                VisibilityGridUser = Visibility.Collapsed;
                OnPropertyChanged(nameof(VisibilityGridUser));
                if (VisibilityGrid == Visibility.Collapsed)
                {
                    VisibilityGrid = Visibility.Visible;
                    OnPropertyChanged(nameof(VisibilityGrid));
                }
            }
            else
            {
                VisibilityGrid = Visibility.Collapsed;
                OnPropertyChanged(nameof(VisibilityGrid));
            }
        }

        private void SetIsCheched(Ermolino64DLL.Model.Product product)
        {
            IsChecked180 = false;
            IsChecked90 = false;
            IsChecked30 = false;
            foreach (var item in product.ProductAndShelfLife)
            {
                switch (item.IdShelfLife)
                {
                    case 1:
                        IsChecked180 = true;
                        break;
                    case 2:
                        IsChecked90 = true;
                        break;
                    case 3:
                        IsChecked30 = true;
                        break;
                }
            }

            OnPropertyChanged(nameof(IsChecked180));
            OnPropertyChanged(nameof(IsChecked90));
            OnPropertyChanged(nameof(IsChecked30));

        }

        public ICommand ButtonCancelEdit => new Command(async () =>
        {
            SelectProduct = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Product.FirstAsync(p => p.Id == SelectProduct.Id);
            foreach (var item1 in SelectProduct.ProductAndShelfLife)
            {
                item1.IdShelfLifeNavigation = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().ShelfLife.Where(sl => sl.Id == item1.IdShelfLife).FirstOrDefaultAsync();
            }
            OnPropertyChanged(nameof(SelectProduct));
        });

        public ICommand ButtonNewProduct => new Command(() => 
        {
            SelectProduct = new Ermolino64DLL.Model.Product
            {
                Carbohydrates = 5.0,
                Fats = 5.0,
                Protein = 5.0,
                Name = "",
                Description = "",
                CookMethod = "",
            };
            ImageSource.Source = null;
            OnPropertyChanged(nameof(SelectProduct));
            OnPropertyChanged(nameof(ImageSource));

            IsReadOnly = !IsReadOnly;
            OnPropertyChanged(nameof(IsReadOnly));

            VisibilitySplitButton = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilitySplitButton));

            VisibilityButtonAddAndCancelAdd = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityButtonAddAndCancelAdd));

            VisibilityTypeProduct = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityTypeProduct));

            Index.Clear();
            OnPropertyChanged(nameof(Index));
        });

        public ICommand ButtonAdd => new Command( async () => 
        {
            if(FlagName && FlagDescription && FlagCookMethod && ComboBoxSelectValue != null)
            {
                if(ImageSource.Source != null)
                {
                    if (FlagFats && FlagProtein && FlagCarbohydrates)
                    {
                        int id = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Product.MaxAsync(p => p.Id) + 1;
                        SelectProduct.TypeProduct = ComboBoxSelectValue.Text;
                        SelectProduct.Id = id;
                        Ermolino64DLL.Model.Product.AddInDataBase(SelectProduct);

                        if (IsChecked180)
                            Ermolino64DLL.Model.ProductAndShelfLife.AddInDataBase(SelectProduct, 1);
                        await Task.Delay(100);
                        if (IsChecked90)
                            Ermolino64DLL.Model.ProductAndShelfLife.AddInDataBase(SelectProduct, 2);
                        await Task.Delay(100);
                        if (IsChecked30)
                            Ermolino64DLL.Model.ProductAndShelfLife.AddInDataBase(SelectProduct, 3);
                        await Task.Delay(100);

                        ButtonCancelAdd.Execute(null);
                    }
                    else
                    {
                        await new MessageDialog("Введите данные насчет калории.", "Нет калории").ShowAsync();
                    }
                }
                else
                {
                    await new MessageDialog("Выберите, пожалуйста, фотографию", "Нет фота").ShowAsync();
                }
            }
            else
            {
                await new MessageDialog("Не вся информация была правильно ведена.\nПроверьте следующие поля: \nИмя\nОписание\nОписание приготовление\nТип продукта", "Ошибка ввода").ShowAsync();
            }
        });

        public ICommand ButtonCancelAdd => new Command(() => 
        {
            IsReadOnly = !IsReadOnly;
            OnPropertyChanged(nameof(IsReadOnly));

            VisibilitySplitButton = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilitySplitButton));

            VisibilityButtonAddAndCancelAdd = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityButtonAddAndCancelAdd));

            VisibilityTypeProduct = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityTypeProduct));

            ButtonAllProduct.Execute(null);
        });
        bool FlagName = true;
        public TextChangedEventHandler TextBoxName => (sender, e) => 
        {
            var text = (sender as TextBox).Text;
            if (text.Length != 0 && text.Length <= 50)
            {
                FlagName = true;
            }
            else FlagName = false;

            SetBorder(sender as TextBox,FlagName);
        };
        bool FlagDescription = false, FlagCookMethod = false;
        public TextChangedEventHandler TextBoxDescription => (sender, e) =>
        {
            var text = (sender as TextBox).Text;
            if (SelectProduct.Description == text)
            {
                if (text.Length != 0)
                {
                    FlagDescription = true;
                }
                else FlagDescription = false;

                SetBorder(sender as TextBox, FlagDescription);
            }
            if(SelectProduct.CookMethod == text)
            {
                if (text.Length != 0)
                {
                    FlagCookMethod = true;
                }
                else FlagCookMethod = false;

                SetBorder(sender as TextBox, FlagCookMethod);
            }

            
        };

        private void SetBorder(TextBox box, bool value) => box.BorderBrush = new SolidColorBrush(value ? Color.FromArgb(255,80,80,80) : Color.FromArgb(255,255,0,0));

        public KeyEventHandler TextBoxKeyDown => (sender, e) => 
        {
            if ((Windows.System.VirtualKey.Number0 <= e.Key && Windows.System.VirtualKey.Number9 >= e.Key) || e.Key == Windows.System.VirtualKey.Back || e.Key == Windows.System.VirtualKey.Delete || (e.Key == Windows.System.VirtualKey.Decimal && !(sender as TextBox).Text.Contains('.')))
                e.Handled = false;
            else e.Handled = true;
        };

        bool FlagFats = false, FlagProtein = false, FlagCarbohydrates = false;
        public TextChangedEventHandler TextBoxCalories => (sender, e) =>
        {
            var text = (sender as TextBox).Text;
            if (text == SelectProduct.Fats.ToString())
            {
                if (text.Length != 0)
                {
                    FlagFats = true;
                }
                else FlagFats = false;
            }
            if (text == SelectProduct.Protein.ToString())
            {
                if (text.Length != 0)
                {
                    FlagProtein = true;
                }
                else FlagProtein = false;
            }
            if (text == SelectProduct.Carbohydrates.ToString())
            {
                if (text.Length != 0)
                {
                    FlagCarbohydrates = true;
                }
                else FlagCarbohydrates = false;
            }
        };
    }
}
