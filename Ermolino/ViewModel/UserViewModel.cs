﻿using Ermolino.Model;
using Ermolino.Model.ClassesForCommunicationWithOtherClasses;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Ermolino.ViewModel
{
    class UserViewModel : BaseViewModel
    {
        public UserViewModel()
        {
            Name = UserPageAndLogin.User.Name;
            Login = UserPageAndLogin.User.Login;
            Password = UserPageAndLogin.User.Password;
            Email = UserPageAndLogin.User.Email;
            Image = UserPageAndLogin.User.Image;
            IsAdmin = UserPageAndLogin.User.IsAdmin;
            Id = UserPageAndLogin.User.Id;

            SetImage();
            GetRecipe();
        }

        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public byte[] Image { get; set; }

        public Tuple<int,string>[] ItemRecipes { get; set; }

        private readonly bool IsAdmin;
        private readonly int Id;
        public Image ImageSource { get; set; } = new Image();
        public ICommand HyperLinkButton => new Command( async () =>
        {
            FileOpenPicker fileOpenPicker = new FileOpenPicker()
            {
               ViewMode = PickerViewMode.List,
            };
            fileOpenPicker.FileTypeFilter.Add(".jpg");
            fileOpenPicker.FileTypeFilter.Add(".jpeg");
            fileOpenPicker.FileTypeFilter.Add(".png");

            var File = await fileOpenPicker.PickSingleFileAsync();

            if (File == null) return;
            using (var inputStream = await File.OpenSequentialReadAsync())
            {
                var readStream = inputStream.AsStreamForRead();
                var bytearray = new byte[readStream.Length];

                await readStream.ReadAsync(bytearray, 0, bytearray.Length);

                Image = bytearray;
            }

            using (var filestream = await File.OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.SetSource(filestream);
                ImageSource.Source = bitmapImage;
            }

            OnPropertyChanged(nameof(ImageSource));
        });

        public ICommand ButtonSaveDataBase => new Command( async () => 
        {
            if (FlagName && FlagLogin && FlagPassword && FlagEmail)
            {
                var db = new Ermolino64DLL.DataBase.ErmolinoDataBaseContext();

                db.Update(new Ermolino64DLL.Model.User
                {
                    Id = Id,
                    Name = Name,
                    Login = Login,
                    Password = Password,
                    Email = Email,
                    Image = Image,
                    IsAdmin = IsAdmin
                });

                await db.SaveChangesAsync();
            }
            else
            {
                await new MessageDialog("Вы где то допустили ошибку.").ShowAsync();
            }
        });

        private async void SetImage() => 
            ImageSource.Source = Image != null ? await ViewDataBaseViewModel.ByteArrayConvertToImageSource(Image) : null;

        private bool FlagName = true;
        public TextChangedEventHandler TextBoxValidationName => (sender, e) => 
        {
            if(sender is TextBox textBox)
            {
                if (textBox.Text.Length <= 30 && textBox.Text.Length != 0)
                    FlagName = true;
                else 
                    FlagName = false;

                SetColorBorderBrush(FlagName, textBox);
            }            
        };

        private bool FlagLogin = true;
        public TextChangedEventHandler TextBoxValidationLogin => (sender, e) =>
        {
            if(sender is TextBox textBox)
            {
                if (textBox.Text.Length <= 30 && textBox.Text.Length != 0)
                    FlagLogin = true;
                else
                    FlagLogin = false;

                SetColorBorderBrush(FlagLogin,textBox);
            }
        };

        private bool FlagPassword = true;
        public TextChangedEventHandler TextBoxValidationPassword => (sender, e) => 
        {
            if(sender is TextBox textBox)
            {
                if (textBox.Text.Length <= 30 && textBox.Text.Length != 0 && CountNumber(textBox.Text) && CountUpper(textBox.Text))
                    FlagPassword = true;
                else FlagPassword = false;

                SetColorBorderBrush(FlagPassword,textBox);
            }
        };

        private bool FlagEmail = true;
        public TextChangedEventHandler TextBoxValidationEmail => (sender, e) =>
        {
            if (sender is TextBox textBox)
            {
                if (textBox.Text.Length <= 30 && textBox.Text.Length != 0 && CheckEmail(textBox.Text))
                    FlagEmail = true;
                else FlagEmail = false;
                
                SetColorBorderBrush(FlagEmail, textBox);
            }
        };

        private bool CheckEmail(string s)
        {
            try
            {
                new System.Net.Mail.MailAddress(s);
                return true;
            }
            catch(FormatException)
            {
                return false;
            }
        }

        private bool CountNumber(string s)
        {
            int count = 0;
            foreach (var item in s)
            {
                if (char.IsDigit(item))
                    count++;
            }

            return count >= 2;
        }

        private bool CountUpper(string s)
        {
            int count = 0;
            foreach (var item in s)
            {
                if (char.IsUpper(item))
                    count++;
            }

            return count >= 1;
        }

        private void SetColorBorderBrush(bool value, TextBox textBox) =>
            textBox.BorderBrush = new SolidColorBrush(value ? Color.FromArgb(255, 80, 80, 80) : Color.FromArgb(255, 255, 0, 0));

        private async void GetRecipe()
        {
            var db = new Ermolino64DLL.DataBase.ErmolinoDataBaseContext();
            ItemRecipes = await db.UserAndRecipe
                                .SelectMany(uar => db.Recipe
                                            .Where(r => r.Id == uar.IdRecipe && uar.IdUser == UserPageAndLogin.User.Id))
                                .Select(a => new Tuple<int, string>(a.Id, a.Name))
                                .ToArrayAsync();

            OnPropertyChanged(nameof(ItemRecipes));
        }

    }
}
