﻿using Ermolino.Model;
using Ermolino.Model.ClassesForCommunicationWithOtherClasses;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Ermolino.ViewModel
{
    class ViewRecipeViewModel : BaseViewModel
    {
        public ViewRecipeViewModel()
        {
            FlyoutParameters = ViewRecipeAndRecipe.TypeRecipe;
            GetFirstRecipe();
        }

        private readonly Style StyleFrame = Application.Current.Resources["FrameProduct"] as Style;
        private string FlyoutParameters { get; set; }
        private readonly StackPanel GlobalStackPanel = new StackPanel();

        public ObservableCollection<StackPanel> StackPanels { get; set; } = new ObservableCollection<StackPanel>();
        public Ermolino64DLL.Model.Recipe SelectRecipe { get; set; }
        public Image ImageSource { get; set; } = new Image();
        public Visibility GridVisibility { get; set; } = Visibility.Collapsed;
        public Visibility FrameVisibility { get; set; } = Visibility.Visible;
        public bool ProgressRingIsActive { get; set; } = true;
        public string ButtonTextFavorites { get; set; } = "Добавить в избраное";

        private async void GetFirstRecipe()
        {
            if (SelectRecipe != null) return;
            await Task.Delay(100);
            SelectRecipe = await new Ermolino64DLL.DataBase.ErmolinoDataBaseContext().Recipe.FirstOrDefaultAsync(r => r.TypeRecipe == FlyoutParameters);
            if (SelectRecipe.Image != null)
            {
                ImageSource.Source = await ViewDataBaseViewModel.ByteArrayConvertToImageSource(SelectRecipe.Image);
            }
            else ImageSource.Source = new BitmapImage(new Uri("ms-appx:///Resources/Image/nophoto.png"));
            OnPropertyChanged(nameof(SelectRecipe));
            OnPropertyChanged(nameof(ImageSource));

            GridVisibility = Visibility.Visible;
            FrameVisibility = Visibility.Collapsed;
            OnPropertyChanged(nameof(GridVisibility));
            OnPropertyChanged(nameof(FrameVisibility));

            ProgressRingIsActive = false;
            OnPropertyChanged(nameof(ProgressRingIsActive));

            BunchRecipeAndUser();
        }

        public EventHandler<object> EventOpenedFlyout => async (sender, e) =>
        {
            await Task.Delay(100);
            await LoadFlyout();
            GlobalStackPanel.Visibility = Visibility.Collapsed;
        };

        public EventHandler<object> EventClosedFlyout => (sender, e) =>
        {
            StackPanels.Clear();
        };
        private async Task LoadFlyout()
        {
            var values = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Recipe.Where(r => r.TypeRecipe == FlyoutParameters).ToArrayAsync();

            StackPanel stack = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            int index = 0;
            while (index < values.Length)
            {
                StackPanel stackPanel = new StackPanel();

                Image image = new Image
                {
                    Source = values[index].Image != null ? await ViewDataBaseViewModel.ByteArrayConvertToImageSource(values[index].Image) : new BitmapImage(new Uri("ms-appx:///Resources/Image/nophoto.png")),
                    Width = 130,
                    Height = 130,
                    Stretch = Stretch.Fill,
                };

                stackPanel.Children.Add(image);

                stackPanel.Children.Add(new TextBlock
                {
                    Text = values[index].Name,
                    TextAlignment = TextAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    TextWrapping = TextWrapping.WrapWholeWords,
                });

                Frame frame = new Frame
                {
                    Content = stackPanel,
                    Width = 130,
                    Height = 170,
                    Style = StyleFrame,
                    Margin = new Thickness(5)
                };

                frame.Tapped += Frame_Tapped;

                ToolTipService.SetToolTip(frame, values[index].Name);

                stack.Children.Add(frame);

                index++;

                if (index % 3 == 0 || index == values.Length)
                {
                    StackPanels.Add(stack);
                    stack = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,
                        HorizontalAlignment = HorizontalAlignment.Center
                    };
                }
            }
        }

        private async void Frame_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (sender is Frame frame)
            {
                var text = ToolTipService.GetToolTip(frame) as string;
                SelectRecipe = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Recipe.Where(r => r.Name == text).FirstOrDefaultAsync();
                if (SelectRecipe.Image != null)
                {
                    ImageSource.Source = await ViewDataBaseViewModel.ByteArrayConvertToImageSource(SelectRecipe.Image);
                }
                else ImageSource.Source = new BitmapImage(new Uri("ms-appx:///Resources/Image/nophoto.png"));

                OnPropertyChanged(nameof(ImageSource));
                OnPropertyChanged(nameof(SelectRecipe));

                BunchRecipeAndUser();
            }
        }

        public ICommand ButtonRecipe => new Command(async (o) => 
        {
            FlyoutParameters = o as string;
            await ClickButton();
        });

        private Task ClickButton()
        {
            GlobalStackPanel.Children.Add(new TextBlock
            {
                Text = "Идёт поиск рецептов...",
                HorizontalAlignment = HorizontalAlignment.Center,
                FontSize = 20,
                FontFamily = new FontFamily("Time New Roman")
            });
            GlobalStackPanel.Children.Add(new ProgressRing
            {
                IsActive = true,
                Width = 200,
                Height = 200
            });
            StackPanels.Add(GlobalStackPanel);
            return Task.CompletedTask;
        }

        public ICommand ButtonAddFavorites => new Command(async() => 
        {
            if(UserPageAndLogin.User != null)
            {
                if (ButtonTextFavorites == "Добавить в избраное")
                {
                    Ermolino64DLL.Model.UserAndRecipe.AddInDataBase(UserPageAndLogin.User.Id, SelectRecipe.Id);
                    ButtonTextFavorites = "Удалить из избранных";
                    OnPropertyChanged(nameof(ButtonTextFavorites));
                }
                else
                {
                    Ermolino64DLL.Model.UserAndRecipe.RemoveFromDataBase(UserPageAndLogin.User.Id, SelectRecipe.Id);
                    ButtonTextFavorites = "Добавить в избраное";
                    OnPropertyChanged(nameof(ButtonTextFavorites));
                }
            }
            else
            {
                await new MessageDialog("Вы не вошли в аккаунт.", "").ShowAsync();
            }
        });

        private async void BunchRecipeAndUser()
        {
            if (UserPageAndLogin.User == null) return;
            var item = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().UserAndRecipe.FirstOrDefaultAsync(uar => uar.IdRecipe == SelectRecipe.Id && UserPageAndLogin.User.Id == uar.IdUser);

            if (item != null)
            {
                ButtonTextFavorites = "Удалить из избранных";
                OnPropertyChanged(nameof(ButtonTextFavorites));
            }
            else
            {
                ButtonTextFavorites = "Добавить в избраное";
                OnPropertyChanged(nameof(ButtonTextFavorites));
            }
        }
    }
}
