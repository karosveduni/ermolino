﻿using Ermolino.Model;
using Ermolino.Model.ClassesForCommunicationWithOtherClasses;
using System;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Ermolino.ViewModel
{
    class MainPageViewModel : BaseViewModel
    {
        public MainPageViewModel()
        {
            MainPageAndLogin.SignIn = (s) =>
            {
                TextHyperLinkButton = s;
                OnPropertyChanged(nameof(TextHyperLinkButton));
            };
            MainPageAndLogin.OpenPageUser = () => ButtonUser.Execute(null);
            MainPageAndLogin.SignInAdmin = (v) =>
            {
                VisibilityDataBase = v;
                OnPropertyChanged(nameof(VisibilityDataBase));
            };
            ViewRecipeAndRecipe.ViewRecipe = (s) => 
            {
                ViewRecipeAndRecipe.TypeRecipe = s;
                SourcePageType = new ViewRecipe();
                OnPropertyChanged(nameof(SourcePageType));
                
            };
            HomeAndProduct.LoadProduct = (s) =>
            {
                HomeAndProduct.TypeProduct = s;
                SourcePageType = new Product();
                TextBlockTitle = "Продукты";
                OnPropertyChanged(nameof(TextBlockTitle));
                OnPropertyChanged(nameof(SourcePageType));
            };
        }
        public bool IsPaneOpen { get; set; } = false;
        public UserControl SourcePageType { get; set; } = new Home();
        public string TextBlockTitle { get; set; } = "Главная";
        public string TextHyperLinkButton { get; set; } = "Вход или Регистрация";
        public Visibility VisibilityDataBase { get; set; } = Visibility.Collapsed;
        public ICommand ButtonOpenMenu => new Command(() =>
        {
            IsPaneOpen = true;
            OnPropertyChanged(nameof(IsPaneOpen));
        });

        public ICommand ButtonItemMenu1 => new Command(() =>
        {
            SourcePageType = new Home();
            TextBlockTitle = "Главная";
            OnPropertyChanged(nameof(TextBlockTitle));
            OnPropertyChanged(nameof(SourcePageType));
        });

        public ICommand ButtonItemMenu2 => new Command(() =>
        {
            SourcePageType = new Product();
            TextBlockTitle = "Продукты";
            OnPropertyChanged(nameof(TextBlockTitle));
            OnPropertyChanged(nameof(SourcePageType));
        });

        public ICommand ButtonItemMenu3 => new Command(() =>
        {
            SourcePageType = new Recipe();
            TextBlockTitle = "Рецепты";
            OnPropertyChanged(nameof(TextBlockTitle));
            OnPropertyChanged(nameof(SourcePageType));
        });

        public ICommand ButtonItemMenu4 => new Command(() =>
        {
            SourcePageType = new Video();
            TextBlockTitle = "Видео";
            OnPropertyChanged(nameof(SourcePageType));
            OnPropertyChanged(nameof(TextBlockTitle));
        });

        public ICommand ButtonItemMenu5 => new Command(() =>
        {
            SourcePageType = new AboutUs();
            TextBlockTitle = "О нас";
            OnPropertyChanged(nameof(SourcePageType));
            OnPropertyChanged(nameof(TextBlockTitle));
        });

        public ICommand ButtonItemMenu6 => new Command(() =>
        {
            SourcePageType = new DataBase();
            TextBlockTitle = "База данных";
            OnPropertyChanged(nameof(SourcePageType));
            OnPropertyChanged(nameof(TextBlockTitle));
        });

        public ICommand ButtonLoginAndRegistration => new Command(() =>
        {
            if (TextHyperLinkButton == "Вход или Регистрация")
            {
                SourcePageType = new LoginAndRegistration();
                TextBlockTitle = "";
                OnPropertyChanged(nameof(SourcePageType));
                OnPropertyChanged(nameof(TextBlockTitle));
            }
            else
            {
                UserPageAndLogin.User = null;
                OnPropertyChanged(nameof(UserPageAndLogin.User));
                MainPageAndLogin.SignIn("Вход или Регистрация");
                if(SourcePageType.GetType() == typeof(User) || SourcePageType.GetType() == typeof(DataBase))
                {
                    SourcePageType = new Home();
                    OnPropertyChanged(nameof(SourcePageType));
                }
                VisibilityDataBase = Visibility.Collapsed;
                OnPropertyChanged(nameof(VisibilityDataBase));
            }
        });

        public ICommand ButtonUser => new Command( async () =>
        {
            if (UserPageAndLogin.User != null)
            {
                SourcePageType = new User();
                TextBlockTitle = "Пользователь";
                OnPropertyChanged(nameof(SourcePageType));
                OnPropertyChanged(nameof(TextBlockTitle));
            }
            else
            {
                var message = new MessageDialog("Войдите в систему или зарегистрируйтесь", "Пользователь не определён");
                message.Commands.Add(new UICommand("Вход или регистрация", (ui) =>
                {
                    ButtonLoginAndRegistration.Execute(null);
                }));
                message.Commands.Add(new UICommand("Закрыть окно"));

                await message.ShowAsync();
            }
        });

        public ICommand AboutProgram => new Command(() => 
        {
            SourcePageType = new AboutProgram();
            TextBlockTitle = "О программе";
            OnPropertyChanged(nameof(SourcePageType));
            OnPropertyChanged(nameof(TextBlockTitle));
        });

    }
}