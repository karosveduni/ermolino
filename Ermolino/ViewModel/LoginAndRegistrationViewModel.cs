﻿using Ermolino.Model;
using System;
using System.Windows.Input;
using System.Linq;
using Ermolino.Model.ClassesForCommunicationWithOtherClasses;
using Microsoft.EntityFrameworkCore;
using Windows.UI.Xaml;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;

namespace Ermolino.ViewModel
{
    class LoginAndRegistrationViewModel : BaseViewModel
    {
        private SHA512 sha = SHA512.Create();
        public string Login { get; set; } = "";
        public string Password { get; set; } = "";
        public Visibility VisibilityStackPanel1 { get; set; } = Visibility.Visible;
        public Visibility VisibilityStackPanel2 { get; set; } = Visibility.Collapsed;
        public bool IsActive { get; set; } = false; 
        public ICommand ButtonLogin => new Command(async () =>
        {
            VisibilityStackPanel1 = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityStackPanel1));

            VisibilityStackPanel2 = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityStackPanel2));

            IsActive = true;
            OnPropertyChanged(nameof(IsActive));

            await Task.Delay(20);

            byte[] password = Encoding.UTF8.GetBytes(Password);

            byte[] resultSHA = sha.ComputeHash(password);

            Password = Convert.ToBase64String(resultSHA);

            var user = Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().User.FirstOrDefault(u => u.Login == Login && u.Password == Password);
            if (user != null)
            {
                MainPageAndLogin.SignIn("Выход");
                UserPageAndLogin.User = user;
                MainPageAndLogin.OpenPageUser();
                if (user.IsAdmin)
                    MainPageAndLogin.SignInAdmin(Windows.UI.Xaml.Visibility.Visible);
            }

            VisibilityStackPanel1 = Visibility.Visible;
            OnPropertyChanged(nameof(VisibilityStackPanel1));

            VisibilityStackPanel2 = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityStackPanel2));

            IsActive = false;
            OnPropertyChanged(nameof(IsActive));
        });
        public ICommand ButtonRegistration => new Command(async () =>
        {
            byte[] password = Encoding.UTF8.GetBytes(Password);

            byte[] resultSHA = sha.ComputeHash(password);

            Password = Convert.ToBase64String(resultSHA);

            int id = Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().User.Max(u => u.Id) + 1;
            Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().User.Add(new Ermolino64DLL.Model.User
            {
                Login = Login,
                Password = Password,
                Email = "",
                Image = null,
                IsAdmin = false,
                Name = "",
                Id = id
            });

            await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().SaveChangesAsync();

        });
    }
}
