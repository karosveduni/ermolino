﻿using Ermolino.Model;
using Ermolino.Model.ClassesForCommunicationWithOtherClasses;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Ermolino.ViewModel
{
    class ProductViewModel : BaseViewModel
    {
        public ProductViewModel()
        {
            FlyoutParameters = HomeAndProduct.TypeProduct;
            GetFirstProduct();
        }

        private readonly Style StyleFrame = Application.Current.Resources["FrameProduct"] as Style;
        public Ermolino64DLL.Model.Product SelectProduct { get; set; }
        public Image ImageSource { get; set; } = new Image();
        public ObservableCollection<StackPanel> StackPanels { get; set; } = new ObservableCollection<StackPanel>();

        private readonly StackPanel GlobalStackPanel = new StackPanel();

        public string FlyoutParameters = "";
        public Visibility VisibilityLoadProduct { get; set; } = Visibility.Visible;
        public Visibility VisibilityGridProduct { get; set; } = Visibility.Collapsed;

        private async void GetFirstProduct()
        {
            if (SelectProduct != null) return;
            await Task.Delay(100);
            SelectProduct = await new Ermolino64DLL.DataBase.ErmolinoDataBaseContext().Product.FirstOrDefaultAsync(p => p.TypeProduct == (FlyoutParameters ?? "Пельмени"));
            if (SelectProduct.Image != null)
            {
                ImageSource.Source = await ViewDataBaseViewModel.ByteArrayConvertToImageSource(SelectProduct.Image);
            }

            SetProductAndShelfLife();

            VisibilityGridProduct = Visibility.Visible;
            VisibilityLoadProduct = Visibility.Collapsed;
            OnPropertyChanged(nameof(VisibilityLoadProduct));
            OnPropertyChanged(nameof(VisibilityGridProduct));
        }

        public EventHandler<object> EventOpenedFlyout => async (sender, e) =>
        {
            await Task.Delay(100);
            await LoadFlyout();
            GlobalStackPanel.Visibility = Visibility.Collapsed;
        };

        public EventHandler<object> EventClosedFlyout => (sender, e) =>
        {
            StackPanels.Clear();
        };

        private async Task LoadFlyout()
        {
            var values = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Product.Where(p => p.TypeProduct == FlyoutParameters).ToArrayAsync();

            StackPanel stack = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Center
            };

            int index = 0;
            while (index < values.Length)
            {
                StackPanel stackPanel = new StackPanel();
                stackPanel.Children.Add(new Image
                {
                    Source = values[index].Image != null ? await ViewDataBaseViewModel.ByteArrayConvertToImageSource(values[index].Image) : new BitmapImage(new Uri("ms-appx:///Resources/Image/nophoto.png")),
                    Width = 130,
                    Height = 130,
                    Stretch = Stretch.Fill,
                });

                stackPanel.Children.Add(new TextBlock 
                {
                    Text = values[index].Name,
                    TextAlignment = TextAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    TextWrapping = TextWrapping.WrapWholeWords,
                });

                Frame frame = new Frame
                {
                    Content = stackPanel,
                    Width = 130,
                    Height = 175,
                    Style = StyleFrame,
                    Margin = new Thickness(5)
                };
                frame.Tapped += Frame_Tapped;

                ToolTipService.SetToolTip(frame, values[index].Name);

                stack.Children.Add(frame);

                index++;

                if (index % 3 == 0 || index == values.Length)
                {
                    StackPanels.Add(stack);
                    stack = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,
                        HorizontalAlignment = HorizontalAlignment.Center
                    };
                }
            }
        }

        private async void Frame_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (sender is Frame frame)
            {
                var text = ToolTipService.GetToolTip(frame) as string;
                SelectProduct = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().Product.Where(p => p.Name == text).FirstOrDefaultAsync();
                if (SelectProduct.Image != null)
                {
                    ImageSource.Source = await ViewDataBaseViewModel.ByteArrayConvertToImageSource(SelectProduct.Image);
                }
                else ImageSource.Source = new BitmapImage(new Uri("ms-appx:///Resources/Image/nophoto.png"));

                SetProductAndShelfLife();
                
                OnPropertyChanged(nameof(ImageSource));
            }
        }

        public ICommand ButtonProduct => new Command(async (o) =>
        {
            FlyoutParameters = o as string;
            await ClickButton();
        });

        private Task ClickButton()
        {
            GlobalStackPanel.Children.Add(new TextBlock 
            {
                Text = "Идёт выгрузка товара...",
                HorizontalAlignment = HorizontalAlignment.Center,
                FontSize = 20,
                FontFamily= new FontFamily("Time New Roman")
            });
            GlobalStackPanel.Children.Add(new ProgressRing
            { 
                IsActive = true,
                Width = 200,
                Height = 200
            });
            StackPanels.Add(GlobalStackPanel);
            return Task.CompletedTask;
        }

        private async void SetProductAndShelfLife()
        {
            SelectProduct.ProductAndShelfLife = Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().ProductAndShelfLife.Where(pasl => pasl.IdProduct == SelectProduct.Id).ToHashSet();
            foreach (var item in SelectProduct.ProductAndShelfLife)
            {
                item.IdShelfLifeNavigation = await Ermolino64DLL.DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().ShelfLife.Where(sl => sl.Id == item.IdShelfLife).FirstOrDefaultAsync();
            }
            SelectProduct.ProductAndShelfLife = SelectProduct.ProductAndShelfLife.OrderBy(pasl => pasl.IdShelfLife).ToArray();
            OnPropertyChanged(nameof(SelectProduct));
        }
    }
}
