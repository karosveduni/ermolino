﻿using System;
using System.Windows.Input;

namespace Ermolino.Model
{
    class Command : ICommand
    {
        Action Action { get; set; }
        Action<object> ObjectAction { get; set; }

        private readonly bool WithParameter = false;

        public Command(Action action) => Action = action;

        public Command(Action<object> action)
        {
            ObjectAction = action;
            WithParameter = true;
        }

        public event EventHandler CanExecuteChanged;
        
        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            if (WithParameter) ObjectAction(parameter);
            else Action();
        }
    }
}
