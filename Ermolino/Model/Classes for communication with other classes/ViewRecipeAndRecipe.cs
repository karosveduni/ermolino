﻿using System;

namespace Ermolino.Model.ClassesForCommunicationWithOtherClasses
{
    class ViewRecipeAndRecipe
    {
        public static Action<string> ViewRecipe { get; set; }
        public static string TypeRecipe { get; set; } = "";
    }
}
