﻿using System;

namespace Ermolino.Model.ClassesForCommunicationWithOtherClasses
{
    class HomeAndProduct
    {
        public static Action<string> LoadProduct { get; set; }
        public static string TypeProduct { get; set; }
    }
}
