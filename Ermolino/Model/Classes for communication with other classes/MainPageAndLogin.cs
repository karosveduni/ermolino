﻿using System;
using Windows.UI.Xaml;

namespace Ermolino.Model.ClassesForCommunicationWithOtherClasses
{
    class MainPageAndLogin
    {
        public static Action<string> SignIn { get; set; }
        public static Action OpenPageUser { get; set; }
        public static Action<Visibility> SignInAdmin { get; set; }
    }
}
