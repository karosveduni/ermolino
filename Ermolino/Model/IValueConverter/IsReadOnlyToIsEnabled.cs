﻿using System;
using Windows.UI.Xaml.Data;

namespace Ermolino.Model.ValueConverter
{
    class IsReadOnlyToIsEnabled : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is bool b)
                return !b;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
