﻿using System;
using Windows.UI.Xaml.Data;

namespace Ermolino.Model.ValueConverter
{
    class CookTimeAndPortionsToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return $"{parameter}: {value}.";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
