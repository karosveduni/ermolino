﻿using System.Collections.Generic;

namespace Ermolino64DLL.Model
{
    public partial class Recipe
    {
        public Recipe()
        {
            UserAndRecipe = new HashSet<UserAndRecipe>();
        }

        public int Id { get; set; }
        public string TypeRecipe { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte TimeCook { get; set; }
        public byte Portions { get; set; }
        public string Ingredients { get; set; }
        public string CookMethod { get; set; }
        public byte[] Image { get; set; }

        public string[] GetArrayIngredients => Ingredients.Split('#');
        public string[] GetArrayCookMethod => CookMethod.Split('#');

        public virtual ICollection<UserAndRecipe> UserAndRecipe { get; set; }
    }
}
