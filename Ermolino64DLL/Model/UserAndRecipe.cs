﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Ermolino64DLL.Model
{
    public partial class UserAndRecipe
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdRecipe { get; set; }

        public virtual Recipe IdRecipeNavigation { get; set; }
        public virtual User IdUserNavigation { get; set; }

        public static async void AddInDataBase(int IdUser, int IdRecipe)
        {
            DataBase.ErmolinoDataBaseContext dataBaseContext = new DataBase.ErmolinoDataBaseContext();

            var item = await DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().UserAndRecipe.Where(uar => uar.IdUser == IdUser && uar.IdRecipe == IdRecipe).FirstOrDefaultAsync();

            if(item == null)
            {
                var value = await DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().UserAndRecipe.ToArrayAsync();

                dataBaseContext.UserAndRecipe.Add(new UserAndRecipe
                {
                    Id = value.Length + 1,
                    IdRecipe = IdRecipe,
                    IdUser = IdUser
                });

                await dataBaseContext.SaveChangesAsync();
            }
        }

        public static async void RemoveFromDataBase(int IdUser, int IdRecipe)
        {
            DataBase.ErmolinoDataBaseContext dataBaseContext = new DataBase.ErmolinoDataBaseContext();

            var item = await DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().UserAndRecipe.Where(uar => uar.IdUser == IdUser && uar.IdRecipe == IdRecipe).FirstOrDefaultAsync();

            if (item != null)
            {
                int Id = await DataBase.ErmolinoDataBaseContext.GetErmolinoDataBaseContext().UserAndRecipe.MaxAsync(uar => uar.Id);

                dataBaseContext.UserAndRecipe.Remove(new UserAndRecipe
                {
                    Id = Id,
                    IdRecipe = IdRecipe,
                    IdUser = IdUser
                });

                await dataBaseContext.SaveChangesAsync();
            }
        }

    }
}
