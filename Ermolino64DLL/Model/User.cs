﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ermolino64DLL.Model
{
    public partial class User
    {
        public User()
        {
            UserAndRecipe = new HashSet<UserAndRecipe>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        public byte[] Image { get; set; }

        public virtual ICollection<UserAndRecipe> UserAndRecipe { get; set; }
    }
}
