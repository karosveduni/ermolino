﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ermolino64DLL.Model
{
    public partial class Product
    {
        public Product()
        {

        }

        public int Id { get; set; }
        public string TypeProduct { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CookMethod { get; set; }
        public double Protein { get; set; }
        public double Fats { get; set; }
        public double Carbohydrates { get; set; }
        public byte[] Image { get; set; }

        public virtual ICollection<ProductAndShelfLife> ProductAndShelfLife { get; set; }

        public static async void AddInDataBase(Product product)
        {
            DataBase.ErmolinoDataBaseContext dataBase = new DataBase.ErmolinoDataBaseContext();

            await dataBase.Product.AddAsync(product);

            await dataBase.SaveChangesAsync();
        }

        public static async void UpdateDataBase(Product product)
        {
            DataBase.ErmolinoDataBaseContext dataBase = new DataBase.ErmolinoDataBaseContext();

            dataBase.Product.Update(product);

            await dataBase.SaveChangesAsync();
        }

        public static async void RemoveFromDataBase(Product product)
        {
            DataBase.ErmolinoDataBaseContext dataBase = new DataBase.ErmolinoDataBaseContext();

            dataBase.Product.Remove(product);

            await dataBase.SaveChangesAsync();
        }
    }
}
