﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ermolino64DLL.Model
{
    public partial class ProductAndShelfLife
    {
        public int Id { get; set; }
        public int IdProduct { get; set; }
        public int IdShelfLife { get; set; }

        public virtual Product IdProductNavigation { get; set; }
        public virtual ShelfLife IdShelfLifeNavigation { get; set; }

        public override string ToString()
        {
            return IdShelfLifeNavigation.ShelfLife1;
        }

        public static async void AddInDataBase(Product product,int idShelfLife)
        {
            DataBase.ErmolinoDataBaseContext dataBaseContext = new DataBase.ErmolinoDataBaseContext();
            int id = dataBaseContext.ProductAndShelfLife.Max(pasl1 => pasl1.Id) + 1;

            ProductAndShelfLife pasl = new ProductAndShelfLife
            {
                Id = id,
                IdProduct = product.Id,
                IdShelfLife = idShelfLife
            };

            if (dataBaseContext.ProductAndShelfLife.FirstOrDefault(pasl1 => pasl1.IdProduct == product.Id && pasl1.IdShelfLife == idShelfLife) == null)
            {
                dataBaseContext.ProductAndShelfLife.Add(pasl);
            }
            else return;

            await dataBaseContext.SaveChangesAsync();
        }

        public static async void DeleteFromDataBase(Product product, int idShelfLife)
        {
            DataBase.ErmolinoDataBaseContext dataBaseContext = new DataBase.ErmolinoDataBaseContext();

            var value = await dataBaseContext.ProductAndShelfLife.FirstOrDefaultAsync(pasl => pasl.IdProduct == product.Id && pasl.IdShelfLife == idShelfLife);

            if (value != null) dataBaseContext.ProductAndShelfLife.Remove(value);
            else return;

            await dataBaseContext.SaveChangesAsync();

        }

    }
}
