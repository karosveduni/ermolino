﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Ermolino64DLL.Model
{
    public partial class ShelfLife
    {
        public ShelfLife()
        {
            ProductAndShelfLife = new HashSet<ProductAndShelfLife>();
        }

        public int Id { get; set; }
        public string ShelfLife1 { get; set; }

        public virtual ICollection<ProductAndShelfLife> ProductAndShelfLife { get; set; }
    }
}
